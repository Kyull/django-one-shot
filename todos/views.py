from django.shortcuts import get_object_or_404, render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoListAddItemForm
# Create your views here.


def todo_list_list(request):
    todolists = TodoList.objects.all()
    context = {
        "todolist_list": todolists,
    }
    return render(request, "todos/todo_list_list.html", context)


def todo_list_detail(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list": todolist,
    }
    return render(request, "todos/todo_list_detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }

    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    todolist = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todolist.id)
    else:
        form = TodoListForm(instance=todolist)

    context = {
        "todolist_object": todolist,
        "todolist_form": form,
    }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    todolist = TodoList.objects.get(id=id)
    if request.method == "POST":
        todolist.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoListAddItemForm(request.POST)
        if form.is_valid():
            task = form.save()
            return redirect("todo_list_detail", id=task.list.id)
    else:
        form = TodoListAddItemForm()

    context = {
        "form": form,
    }

    return render(request, "todos/add_item.html", context)


def todo_item_update(request, id):
    todo_item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoListAddItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.list.id)
    else:
        form = TodoListAddItemForm(instance=todo_item)

    context = {
        "todo_item": todo_item,
        "form": form,
    }
    return render(request, "todos/edit_item.html", context)
